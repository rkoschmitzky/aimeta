############## datagui.py ###################################
# module includes the Qt interface for handling metadata and
# indicator assignments
#
# Creation Date:
# 2015-06-1
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# General Imports
import collections
from functools import partial
import logging
import os
import sys

# Package Imports
from aimeta.src.utils import ValidateAiDataType
from aimeta.ui.qthelper import QtGui, QtCore, Signal, getWidgetUnderCursor
from aimeta.ui.gui.descriptiongui import DescriptionGUI

_parentDir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
_iconsDir = os.path.join(_parentDir, 'icons')

# TODO add logging
# TODO add missing docstrings
class DataGUI(QtGui.QDialog):

    def __init__(self, parent=None, drivers=None, AOVs=None):
        super(DataGUI, self).__init__(parent)
        self._drivers = drivers
        self._AOVs = AOVs
        self._parent = parent

    def build(self, drivers=None, AOVs=None):
        # initializing and storing the instances
        if drivers is None:
            self.__metaDataTab = MetadataTab(parent=self._parent, drivers=self._drivers)
        else:
            self.__metaDataTab = MetadataTab(parent=self._parent, drivers=drivers)
        if AOVs is None:
            self.__indicatorTab = IndicatorTab(parent=self._parent, AOVs=self._AOVs)
        else:
            self.__indicatorTab = IndicatorTab(parent=self._parent, AOVs=AOVs)
        self.__collectorTab = CollectorTab(parent=self._parent)
        # initializing UI

        #create main layout
        self.mainContainer = QtGui.QVBoxLayout()
        self.mainLogo = QtGui.QLabel()
        self.mainLogo.setStyleSheet("background-color: rgb(45, 45, 45);\
                                     border-style: inset; \
                                     border-width: 1px; \
                                     border-color: rgb(20, 20, 20); ")
        #self.mainLogo.setPixmap(os.path.join(_iconsDir, 'aiMeta_main_45px.png'))
        # PyQt testing purpose
        self.mainLogo.setPixmap(QtGui.QPixmap(os.path.join(_iconsDir, 'aiMeta_main_45px.png')))
        self.mainLogo.setAlignment(QtCore.Qt.AlignHCenter)
        self.mainContainer.addWidget(self.mainLogo)

        # create main TabWidget and adding the contents
        self.mainTab = QtGui.QTabWidget()
        self.mainTab.addTab(self.__metaDataTab, 'Custom Metadata')
        self.mainTab.addTab(self.__indicatorTab, 'Indicators')
        self.mainTab.addTab(self.__collectorTab, 'Collector')

        self.mainContainer.addWidget(self.mainTab)
        self.setLayout(self.mainContainer)

    def showUI(self):
        """ builds and shows the interface

        """
        self.build()
        self.show()

    def closeEvent(self, event):
        qtype = event.type()
        if qtype == QtCore.QEvent.Close:
            self.deleteLater()
        else:
            pass

    @property
    def metaData(self):
        return self.__metaDataTab.metaData

    @property
    def indicators(self):
        return self.__indicatorTab.indicators

    @property
    def settings(self):
        return self.__collectorTab.settings

    @property
    def driverDropDown(self):
        return self.__metaDataTab.driverDropdown

    @property
    def executeMetaDataButton(self):
        """ holds the widget of the executeButton for the metaData tab

        :return: QPushButton instance
        """
        return self.__metaDataTab.executeButton

    @property
    def executeIndicatorsButton(self):
        """ holds the widget of the executeButton for the indicator tab

        :return: QPushButton instance
        """
        return self.__indicatorTab.executeButton

    @property
    def executeSettingsButton(self):
        """ holds the widget of the executeButton for the collector tab

        :return: QPushButton instance
        """
        return self.__collectorTab.executeButton

    @property
    def metaDataTab(self):
        """ holds the widget of the metaDataTab

        :return: QWidgetInstance
        """
        return self.__metaDataTab

    @property
    def collectorTab(self):
        """ holds the widget of the collectorTab

        :return: QWidgetInstance
        """
        return self.__collectorTab

    @property
    def indicatorTab(self):
        """ holds the widget of the indicatorTab

        :return: QWidgetInstance
        """
        return self.__indicatorTab

    @property
    def descriptionWindow(self):
        """ holds the DescriptionWidget instance

        :return: QDialog instance
        """
        return self.__indicatorTab._gui.container

    @property
    def descriptionObject(self):
        """ holds the DescriptionGUI instance

        :return: QDialog instance
        """
        return self.__indicatorTab._gui


class MetadataTab(QtGui.QWidget):
    def __init__(self, parent = None, drivers = None):
        super(MetadataTab, self).__init__(parent)

        self.__metaData = {}
        self.__metaDataStates = [True, True]
        self.__metaDataNameFields = []
        self.__datatypes = ['int', 'float', 'point2', 'string', 'matrix']
        self._discard = False

        self.mainLayout = QtGui.QVBoxLayout()

        # driver layout and its widgets
        self.driverInputLayout = QtGui.QHBoxLayout()
        self.driverDropdown = QtGui.QComboBox()

        # creating the real drivers
        if drivers is not None:
            for driver in drivers:
                self.driverDropdown.addItem('%s' % driver)
        # create add button and setting its styles
        self.addMetadataTemplateButton = QtGui.QPushButton()
        _size = 32
        self.addMetadataTemplateButton.setFixedSize(_size, _size)
        _icon = QtGui.QIcon(os.path.join(_iconsDir, 'Button_plus_48px.png'))
        self.addMetadataTemplateButton.setIcon(_icon)
        self.addMetadataTemplateButton.setIconSize(QtCore.QSize(_size, _size))
        self.addMetadataTemplateButton.setFlat(True)
        # adding its widget
        self.driverInputLayout.addWidget(self.driverDropdown)
        self.driverInputLayout.addWidget(self.addMetadataTemplateButton)

        self.metadataGroupBox = QtGui.QGroupBox('Included Data')
        self.dataTemplateLayout = QtGui.QVBoxLayout()
        self.metadataGroupBox.setLayout(self.dataTemplateLayout)

        self.executeMetaDataButton = QtGui.QPushButton('Create MetaData')

        # creating the dataLayout
        self.dataLayout = QtGui.QVBoxLayout()
        self.dataLayout.addWidget(self.metadataGroupBox)

        self.mainLayout.insertLayout(0, self.driverInputLayout)
        self.mainLayout.addWidget(self.metadataGroupBox)

        self.mainLayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding))
        self.mainLayout.addWidget(self.executeMetaDataButton)
        self.setLayout(self.mainLayout)
        self._establishConnection()

        self.__driverDropDownIndex = self.driverDropdown.currentIndex()

    def _establishConnection(self):
        """ connects some of the main signals

        """
        self.addMetadataTemplateButton.clicked.connect(self.addMetadataTemplate)

    def addMetadataTemplate(self, nameValue=None, dataType=None, dataValue=None):
        """ adds the UI template for inserting metadata

        :param nameValue: metadata entry name
        :type nameValue: string
        :param dataType: metadata dataType
        :type dataType: string
        :param dataValue: value that has to be assigned to the entry name
        :type dataValue: depending on the dataType string, int or float
        """
        _layout = QtGui.QHBoxLayout()

        # creating the nameField
        nameField = QtGui.QLineEdit()
        nameField.textChanged.connect(partial(self._nameValidate, nameField))
        self.__metaDataNameFields.append(nameField)

        typesDropdown = QtGui.QComboBox()

        # initializing the valueField to assign its address to the nameField
        valueField = QtGui.QLineEdit()
        nameField.setProperty('valueWidget', valueField)
        for type in self.__datatypes:
            typesDropdown.addItem(type)
        # creating the remove button
        removeButton = QtGui.QPushButton()
        removeButton.setProperty('nameWidget', nameField)
        removeButton.setFixedSize(20, 20)
        _icon = QtGui.QPixmap(os.path.join(_iconsDir, 'Button_minus_r_32px.png'))
        removeButton.setIcon(_icon)
        removeButton.setIconSize(QtCore.QSize(20, 20))
        removeButton.setFlat(True)
        removeButton.clicked.connect(partial(self.removeMetaDataTemplate, removeButton))
        # adding the object to our valueFields property that we now where to query what kind of datatype we want
        # to check
        valueField.setProperty('dataType', typesDropdown)
        valueField.textChanged.connect(partial(self._typeValidate, valueField))
        # adding widgets to the sublayout
        _layout.addWidget(nameField)
        _layout.addWidget(typesDropdown)
        _layout.addWidget(valueField)
        _layout.addWidget(removeButton)
        # adding the sublayout to the given layout
        self.dataTemplateLayout.addLayout(_layout)

        # adding the contents to the widgets
        if nameValue is not None:
            nameField.setText(nameValue)
        if dataType is not None:
            _index = typesDropdown.findText(dataType)
            typesDropdown.setCurrentIndex(_index)
        if dataValue is not None:
            valueField.setText(dataValue)

    def removeMetaDataTemplate(self, widget):
        """ removes the UI template

        :param widget: QWidget that holds the "nameWidget" property
        """
        _layouts = widget.parent().layout().children()
        for layout in _layouts:
            if _hasWidget(layout, widget):
                _todelete = _layouts[_layouts.index(layout)]
                _clearLayout(_todelete)
                self.__metaDataNameFields.remove(widget.property('nameWidget'))

    def _dataType(self, valueFieldWidget, *args):
        """

        :param valueFieldWidget: QWidget that holds the "dataType" property, where it stores the QComboBox instance
        :return: returns the dataType set in the specific QComboBox widget
        :rtype: string
        """
        return valueFieldWidget.property('dataType').itemText(int(valueFieldWidget.property('dataType').currentIndex()))

    def driverChangeConfirm(self):
        """ opens a QMessageBox and waits for user confirmation

        """
        if self.__driverDropDownIndex != self.driverDropdown.currentIndex():
            flags = QtGui.QMessageBox.StandardButton.Yes
            flags |= QtGui.QMessageBox.StandardButton.No
            question = "Changing the driver will discard your changes. Do you want to continue? "
            response = QtGui.QMessageBox.question(self, "Question",
                                                  question,
                                                  flags)
            if response == QtGui.QMessageBox.Yes:
                self._discard = True

    def onDriverChanged(self, func, *args):
        """ specific method to assign a function when the driver QComboBox currentIndexChanged signal is emitted;
        this includes the QMessageBox confirm dialog popup

        :param func: functionname
        :tape func: string
        """
        _index = self.driverDropdown.currentIndex()
        self.driverChangeConfirm()
        if self._discard:
            self.clearMetaDataGroup()
            self.__driverDropDownIndex = _index
            func()
        else:
            self.driverDropdown.setCurrentIndex(self.__driverDropDownIndex)
            self.__driverDropDownIndex = self.driverDropdown.currentIndex()
        self._discard = False

    def clearMetaDataGroup(self):
        """ deletes all the medatata UI templates for the current set driver

        """
        _layout = self.metadataGroupBox.layout()
        _clearLayout(_layout)
        self.__metaDataNameFields = []

    def _nameExists(self, *args):
        """ iterates through all the custom set metadata entry names from the UI and checks if the entry name already
        exists

        :return: returns True if the set entry name already and False if not
        :rtype: bool
        """
        _duplicates = [item for item, count in collections.Counter([str(name.text()) for name in self.metaDataNameFields]).items() if count > 1]
        _duplicates = [duplicate for duplicate in _duplicates if not duplicate == '']
        if _duplicates == []:
            return False
        else:
            return True

    def _nameValidate(self, QLineEditWidget, *args):
        """ validates if the input in the metadata entry input widget is unique, if not it will alter the style
        of the current used QLineEditWidget

        :param QLineEditWidget: QLineEditWidget
        """
        if self._nameExists():
            self._setMetaDataStates(state1 = False)
            _switchColor(False, QLineEditWidget)
        else:
            self._setMetaDataStates(state1 = True)
            _switchColor(True, QLineEditWidget)

    def _typeValidate(self, QLineEditWidget, *args):
        """ validates if the input in the value input is the correct for the corresponding set dataType,
        if not it will alter the style of the current used QLineEditWidget

        :param QLineEditWidget: QLineEditWidget
        """
        _input = str(QLineEditWidget.text())
        if _input == '':
            _switchColor(True, QLineEditWidget)
            self._setMetaDataStates(state2 = True)
            return
        if self._dataType(QLineEditWidget) == 'int':
            _state = ValidateAiDataType.int(_input)
            _switchColor(_state, QLineEditWidget)
        if self._dataType(QLineEditWidget) == 'float':
            _state = ValidateAiDataType.float(_input)
            _switchColor(_state, QLineEditWidget)
        if self._dataType(QLineEditWidget) == 'string':
            _state = ValidateAiDataType.string(_input)
            _switchColor(_state, QLineEditWidget)
        if self._dataType(QLineEditWidget) == 'point2':
            _state = ValidateAiDataType.point2(_input)
            _switchColor(_state, QLineEditWidget)
        if self._dataType(QLineEditWidget) == 'matrix':
            _state = ValidateAiDataType.matrix(_input)
            _switchColor(_state, QLineEditWidget)
        self._setMetaDataStates(state2 = _state)

    def _collectData(self):
        """ queries the current set data in the metadata tab

        :return: returns a dictonary in that form
        {#index of the metadata template : 'name' : #queried name, 'dataType' : queried dataType, 'value' : #queried value}}
        """
        _data = {}
        i = 0
        for nameField in self.metaDataNameFields:
            valueWidget = nameField.property('valueWidget')
            typesDropDown = valueWidget.property('dataType')
            # ordering for better usage later on
            _subDict = {}
            _subDict['dataType'] = '%s' % str(typesDropDown.itemText(typesDropDown.currentIndex()))
            _subDict['name'] = '%s' % str(nameField.text())
            _subDict['value'] = '%s' % str(valueWidget.text())
            _data[i] = _subDict
            i += 1
        self.metaData = _data

    def _setMetaDataStates(self, state1=None, state2=None):
        """ set internal states

        :param state1: state 1
        :type state1: bool
        :param state2: state 2
        :type state2: bool
        """
        _states = self.__metaDataStates
        if state1 is not None:
            _states[0] = state1
        if state2 is not None:
            _states[1] = state2
        self.__metaDataStates = _states

    #####################
    # getters & setters #
    #####################

    @property
    def metaData(self):
        """ returns the current data set in the metadata UI templates

        :return: returns a dictonary in that form
        {#index of the metadata template : 'name' : #queried name, 'dataType' : queried dataType, 'value' : #queried value}}
        """
        self._collectData()
        return self.__metaData

    @metaData.setter
    def metaData(self, metaData):
        """ sets the protected metaData member

        :param metaData:
        :return:
        """
        if not isinstance(metaData, dict):
            # TODO write something useful
            print 'need a dict got bla instead'
        else:
            self.__metaData = metaData

    @property
    def metaDataNameFields(self):
        """ holds the current widgets for metaData entries

        :return: returns list with QLineEdit widgets
        """
        return self.__metaDataNameFields

    @property
    def metaDataStates(self):
        """ returns the current metadata validation states

        :return: list with entryName state and data state
        """
        return self.__metaDataStates

    @property
    def executeButton(self):
        """ holds the widget of the executeButton

        :return: QPushButton instance
        """
        return self.executeMetaDataButton

    @property
    def currentDriver(self):
        """ holds the name of the current driver in the dropdown

        :return:
        """
        return str(self.driverDropdown.currentText())

class IndicatorTab(QtGui.QWidget):
    def __init__(self, parent=None, AOVs=None):
        super(IndicatorTab, self).__init__(parent)
        # hardcoded at the moment, we should change that and make it possible to read that from a settings file
        self.__possibleIndicators = ['others', 'beauty', 'id', 'occlusion', 'depth', 'tech']
        self.__indicators = {}
        self.__AOVs = []
        # we have to assign a beforeBindFunction to allow a custom function call before the context menu
        # on the indicator dropdown is registred
        # for example: this will allow us to set class members before the context menu is called
        self.__beforeBindFunction = None
        self.__initialDescription = ''
        self.__indicatorDropDowns = []

        self._gui = DescriptionGUI(parent)

        self.mainLayout = QtGui.QVBoxLayout()

        self.AOVGroupBox = QtGui.QGroupBox('Scene AOVs')
        self.AOVTemplateLayout = QtGui.QVBoxLayout()
        self.AOVGroupBox.setLayout(self.AOVTemplateLayout)

        self.mainLayout.addWidget(self.AOVGroupBox)

        self.executeIndicatorButton = QtGui.QPushButton('Set Indicators')

        self.mainLayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding))
        self.mainLayout.addWidget(self.executeIndicatorButton)
        self.setLayout(self.mainLayout)
        self._establishConnections()

        if AOVs is not None:
            for aov in AOVs:
                self.addAOVTemplate('%s' % aov)

        self._initConextMenu()

    def _establishConnections(self):
        pass

    def addAOVTemplate(self, AOVName, indicator=None):
        """ adds the indicator UI template

        :param AOVName: name of the AOV
        :type AOVName: string
        :param indicator: if set name of the indicator, otherwise the default indicator
        :type indicator: string
        """
        # add layout
        _layout = QtGui.QHBoxLayout()
        # add label with AOVName
        AOVLabel = QtGui.QLabel(AOVName)
        self.__AOVs.append(AOVLabel)
        # add dropdown
        indicatorDropDown = QtGui.QComboBox()
        for indicator in self.__possibleIndicators:
            indicatorDropDown.addItem(indicator)
            AOVLabel.setProperty('indicatorWidget', indicatorDropDown)
        self.__indicatorDropDowns.append(indicatorDropDown)
        _layout.addWidget(AOVLabel)
        _layout.addWidget(indicatorDropDown)

        self.AOVTemplateLayout.addLayout(_layout)

    def bindMenuToIndicatorDropdowns(self, beforeBindFunction=None):
        """ installs the custom contextMenu to all the indicator QComboBox widgets

        :param beforeBindFunction: function that has to be executed before the context menu will be shown
        :type beforeBindFunction: string
        """
        self.__beforeBindFunction = beforeBindFunction
        for indicatorDropDown in self.__indicatorDropDowns:
            indicatorDropDown.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            indicatorDropDown.customContextMenuRequested.connect(partial(self.showContextMenu, beforeBindFunction))
        self.__beforeBindFunction = None

    def showContextMenu(self, beforeOpenFunc, *args):
        """ shows the context menu

        :param beforeOpenFunc: function that has to be executed before the context menu will be shown
        :type beforeOpenFunc: string
        """
        if beforeOpenFunc is not None:
            beforeOpenFunc()
        _dropDown = getWidgetUnderCursor()
        self.edit.triggered.connect(partial(self.openDescriptionGUI, self._currentIndicator(_dropDown)))
        self.menu.exec_(QtGui.QCursor.pos())

    def _initConextMenu(self):
        """ initializes the context menu

        """
        self.menu = QtGui.QMenu(self)
        self.edit = QtGui.QAction("&edit description", self)
        self.menu.addAction(self.edit)

    def openDescriptionGUI(self, indicatorName=None):
        """ shows the description window

        :param indicatorName: if set it will alter the label of the included QGroupBox label otherwise its None
        :type indicatorName: string
        :return:
        """
        # we have to use self._gui instead of _gui only because the garbage collection will remove the object
        self._gui.indicatorName = indicatorName
        self._gui.showUI(self.initialDescription)

    def _currentIndicator(self, QComboBoxWidget):
        """ gets the text of the current index of the QComboBox widget

        :param QComboBoxWidget: QComboBoxWidget
        :type QComboBoxWidget: QComboBoxWidget instance
        :return: text at index
        :rtype: string
        """
        return QComboBoxWidget.itemText(QComboBoxWidget.currentIndex())

    def _indicator(self, QComboBoxWidget, *args):
        """ gets the AOV label text of the via property assigned indicator widget

        :param QComboBoxWidget: QWidget
        :type QComboBoxWidget: QWidget instance
        """
        return QComboBoxWidget.property('indicatorWidget').itemText(int(QComboBoxWidget.property('indicatorWidget').currentIndex()))

    def indicatorNameUnderPointer(self):
        """ gets the indicator that is positioned under cursor

        :return: returns the indicator text queried from the QComboBox widget
        :rtype: string
        """
        _dropDown = getWidgetUnderCursor()
        return self._currentIndicator(_dropDown)

    def _getIndicatorWidget(self, aovText):
        """ gets the indicator QComboBox widget by the corresponding AOV name

        :param aovText:
        :return:
        """
        # this only works with unique AOV names
        return [_aov.property('indicatorWidget') for _aov in self.__AOVs if str(_aov.text()) == aovText][0]

    def setIndicatorDropDown(self, aovText, indicatorName):
        """ sets the item of a QComboBox widget by a given AOV label text the the index where the indicatorName is positioned

        :param aovText: text of the AOV widget label
        :type aovText: string
        :param indicatorName: name of the indicator to set
        :type indicatorName: string
        :return:
        """
        _dropDown = self._getIndicatorWidget(aovText)
        _dropDown.setCurrentIndex(_dropDown.findText(indicatorName))

    @property
    def indicators(self):
        """ holds a dictonary including the AOV names as keys and its indicators as values

        :return: dict
        :rtype: dict
        """
        self.__indicators = {}
        for AOV in self.__AOVs:
            self.__indicators[str(AOV.text())] = self._indicator(AOV)
        return self.__indicators

    @property
    def executeButton(self):
        """ holds the QPushButton instance for setting the indicators

        :return: QPushButton instance
        """
        return self.executeIndicatorButton

    @property
    def descriptionEditAction(self):
        """ holds the action of the custom context menu for opening the description window

        :return:
        """
        return self.edit

    @property
    def initialDescription(self):
        """ holds the last set initial description

        :return: description text
        :rtype: string
        """
        return self.__initialDescription

    @initialDescription.setter
    def initialDescription(self, description):
        """ sets the internal ininitial description class member

        :param description: description text
        :return:
        """
        self.__initialDescription = description


class CollectorTab(QtGui.QWidget):
    def __init__(self, parent=None):
        super(CollectorTab, self).__init__(parent)
        self.__widgets = {}
        self.__settings = {}
        self.__QCheckboxes = []
        self.mainLayout = QtGui.QVBoxLayout()

        self.AutoCollectGroupBox = QtGui.QGroupBox('Auto-Collect')
        self.inputFieldsLayout = QtGui.QGridLayout()
        self.checkBoxFieldsLayout = QtGui.QGridLayout()

        self.subLayout = QtGui.QVBoxLayout()
        self.AutoCollectGroupBox.setLayout(self.subLayout)

        # defining input fields and checkboxes
        # have to find a smarter way, currently its too much boilerplate
        self.prefix = QtGui.QLabel('Prefix')
        self.operator = QtGui.QLabel('Operator')
        self.projectName = QtGui.QLabel('Project Name')
        self.projectNumber = QtGui.QLabel('Project Number')
        self.separator = QtGui.QFrame()
        self.separator.setFrameShape(QtGui.QFrame.HLine)
        self.separator.setFrameShadow(QtGui.QFrame.Sunken)
        self.includeCollectionData = QtGui.QCheckBox('Include Custom Metadata')
        self.__QCheckboxes.append(self.includeCollectionData)
        self.includeOperator = QtGui.QCheckBox('Include Operator')
        self.__QCheckboxes.append(self.includeOperator)
        self.includeProjectName = QtGui.QCheckBox('Include Project Name')
        self.__QCheckboxes.append(self.includeProjectName)
        self.includeProjectNumber = QtGui.QCheckBox('Include Project Number')
        self.__QCheckboxes.append(self.includeProjectNumber)
        self.includeSceneName = QtGui.QCheckBox('Include Scene Name')
        self.__QCheckboxes.append(self.includeSceneName)
        self.includeIndicators = QtGui.QCheckBox('Include Indicators')
        self.__QCheckboxes.append(self.includeIndicators)
        self.includeAlembic = QtGui.QCheckBox('Include Alembic Paths')
        self.__QCheckboxes.append(self.includeAlembic)
        self.includeReferences = QtGui.QCheckBox('Include Maya Reference Paths')
        self.__QCheckboxes.append(self.includeReferences)
        self.includeMayaVersion = QtGui.QCheckBox('Include Maya Version')
        self.__QCheckboxes.append(self.includeMayaVersion)
        self.includeOS = QtGui.QCheckBox('Include OS Infos')
        self.__QCheckboxes.append(self.includeOS)

        self.prefixEdit = QtGui.QLineEdit()
        self.operatorEdit = QtGui.QLineEdit()
        self.projectNameEdit = QtGui.QLineEdit()
        self.projectNumberEdit = QtGui.QLineEdit()

        # adding to first layout
        self.inputFieldsLayout.addWidget(self.prefix, 1, 0)
        self.inputFieldsLayout.addWidget(self.prefixEdit, 1, 1)
        self.inputFieldsLayout.addWidget(self.operator, 1, 2)
        self.inputFieldsLayout.addWidget(self.operatorEdit, 1, 3)
        self.inputFieldsLayout.addWidget(self.projectName, 2, 0)
        self.inputFieldsLayout.addWidget(self.projectNameEdit, 2, 1)
        self.inputFieldsLayout.addWidget(self.projectNumber, 2, 2)
        self.inputFieldsLayout.addWidget(self.projectNumberEdit, 2, 3)

        # adding to groupbox layout
        self.subLayout.insertLayout(0, self.inputFieldsLayout)
        self.subLayout.addWidget(self.separator)

        self.checkBoxFieldsLayout.addWidget(self.includeCollectionData, 1, 0)
        self.checkBoxFieldsLayout.addWidget(self.includeIndicators, 1, 1)
        self.checkBoxFieldsLayout.addWidget(self.includeProjectName, 1,2)
        self.checkBoxFieldsLayout.addWidget(self.includeProjectNumber, 1,3)
        self.checkBoxFieldsLayout.addWidget(self.includeSceneName, 2, 0)
        self.checkBoxFieldsLayout.addWidget(self.includeOperator, 2, 1)
        self.checkBoxFieldsLayout.addWidget(self.includeAlembic, 2, 2)
        self.checkBoxFieldsLayout.addWidget(self.includeReferences, 2, 3)
        self.checkBoxFieldsLayout.addWidget(self.includeMayaVersion, 3 , 0)
        self.checkBoxFieldsLayout.addWidget(self.includeOS, 3, 1)
        self.subLayout.insertLayout(2, self.checkBoxFieldsLayout)

        self.subLayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding))
        self.mainLayout.addWidget(self.AutoCollectGroupBox)
        self.storeSettingsButton = QtGui.QPushButton('Collect')

        self.mainLayout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding))
        self.mainLayout.addWidget(self.storeSettingsButton)
        self.setLayout(self.mainLayout)

        self._establishConnections()

    def _establishConnections(self):
        """ connects the main signals

        """
        self.executeButton.clicked.connect(partial(self.getSettings, True))

    def _collect(self):
        """ sets the current settings internally, queried from the auto-collect tab content
        using this form {#widgets text : #widget instance}

        """
        self.prefixEdit.setProperty('value', str(self.prefixEdit.text()))
        self.operatorEdit.setProperty('value', str(self.operatorEdit.text()))
        self.projectNameEdit.setProperty('value', str(self.projectNameEdit.text()))
        self.projectNumberEdit.setProperty('value', str(self.projectNumberEdit.text()))
        _data = {'prefix' : self.prefixEdit,
                 'operator' : self.operatorEdit,
                 'project_name' : self.projectNameEdit,
                 'project_number' : self.projectNumberEdit
                 }
        for widget in self.__QCheckboxes:
            widget.setProperty('value', bool(widget.checkState()))
            _data[str(widget.text()).replace(' ', '_').lower()] = widget
        self.__settings = _data

    def getSettings(self, values=False):
        """ returns the queried settings

        :param values: if set it will return a dictonary with that form {#widgets text : #widgets queried value} otherweise
        it will return a dictonary with this form {#widgets text : #widget instance}
        :return: dict
        :type: dict
        """
        self._collect()
        if not values:
            return self.__settings
        else:
            # Python >= 2.7
            return {key: value.property('value')for key, value in self.__settings.items()}
            # only for testing purpose
            #_dict = {}
            #for key, value in self.__settings.items():
            #    _dict[key] = value.property('value')
            #return _dict

    @property
    def executeButton(self):
        """ holds the QPushButon instance for store settings execution

        :return: QPushButton instance
        """
        return self.storeSettingsButton

def _clearLayout(layout):
    """ deletes all widgets in a given layout

    :param layout: QLayout
    """
    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()
            else:
                _clearLayout(item.layout())


def _hasWidget(layout, widget):
    """ checks if layout has a specific widget

    :param layout: QLayout instance
    :param widget: QWidget instance
    """
    for i in range(layout.count()):
        if widget == layout.itemAt(i).widget():
            return True
    return False


def _switchColor(validState, widget, *args):
    """ switches background color by given state to red or back to its original color

    :param validState: if True change color, if False change back to original color
    :param widget: Widget to set its color
    """
    if not validState:
        widget.setStyleSheet("background-color: rgb(100, 0, 0);")
    else:
        widget.setStyleSheet("")


def main():
    def onBind():
        print loaderWindow.indicatorTab.indicatorNameUnderPointer()

    def onSave(description):
        print 'stored description %s' % description
    app = QtGui.QApplication(sys.argv)
    _drivers = ['aiDefaultDriver', 'aiAOVDriver1']
    _aovs = ['direct_specular1', 'direct_diffuse', 'aiAOV_ID']
    #create main window
    loaderWindow = DataGUI(drivers=_drivers, AOVs=_aovs)
    loaderWindow.build()
    loaderWindow.descriptionWindow.saveClicked.connect(onSave)
    loaderWindow.indicatorTab.bindMenuToIndicatorDropdowns(onBind)
    loaderWindow.showUI()
    loaderWindow.setFocus()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
else:
    pass
