############## descriptiongui.py ###################################
# module includes the Qt interface to edit indicator descriptions
#
# Creation Date:
# 2015-06-1
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# global imports
import sys

# Package Imports
from aimeta.ui.qthelper import QtGui, QtCore, Signal


class DescriptionWidget(QtGui.QDialog):
    saveClicked = Signal(list)

class DescriptionGUI(QtGui.QDialog):

    def __init__(self, parent=None, indicatorName=None, description=None):
        super(DescriptionGUI, self).__init__(parent)

        self.__indicatorName = indicatorName
        self._indicatorLabel = 'Edit description for indicator: %s' % self.__indicatorName
        self.build()

    def build(self, description=''):
        """ builds the interface and initializing all of its widgets

        :param description:  initial description to add to
        :type description: string
        """
        self.container = DescriptionWidget()
        self.mainLayout = QtGui.QVBoxLayout(self.container)
        self.descriptionBox = QtGui.QGroupBox(self._indicatorLabel)

        self.descriptionBoxLayout = QtGui.QVBoxLayout()
        self.descriptionEdit = QtGui.QTextEdit()
        self.descriptionEdit.setText(description)
        self.saveButton = QtGui.QPushButton('Save')
        self.saveButton.clicked.connect(self._onSaveClicked)
        self.descriptionBoxLayout.addWidget(self.descriptionEdit)
        self.descriptionBoxLayout.addWidget(self.saveButton)
        self.descriptionBox.setLayout(self.descriptionBoxLayout)

        self.mainLayout.addWidget(self.descriptionBox)
        self.setLayout(self.mainLayout)

    def _onSaveClicked(self):
        """ emits the custom saveClicked signal

        """
        self.container.saveClicked.emit([self.indicatorName, self.currentDescription])

    def showUI(self, description=None):
        """ shows the UI with or without an initial description

        :param description: if set it adds the description
        :type description: string
        """
        if description is not None:
            self.currentDescription = description
        self.show()

    def setDescription(self, description):
        """ sets the current description

        :param description: description text
        :type description: string
        """
        self.currentDescription = description

    @property
    def indicatorName(self):
        """ holds the set indicator name

        :return: indicator name
        :rtype: string
        """
        return self.__indicatorName

    @indicatorName.setter
    def indicatorName(self, indicatorName):
        """ sets the indicator name

        :param indicatorName: indicator name
        :type indicatorName: string
        """
        self.__indicatorName = indicatorName
        self._indicatorLabel = 'Edit description for indicator: %s' % self.__indicatorName
        self.descriptionBox.setTitle(self._indicatorLabel)

    @property
    def executeButton(self):
        """ holds the save button instance

        :return: QPushButton instance
        """
        return self.saveButton

    @property
    def currentDescription(self):
        """ holds the current set description in the UI and returns it as text

        :return: description text
        :rtype: string
        """
        return self.descriptionEdit.toPlainText()

    @currentDescription.setter
    def currentDescription(self, description):
        """ sets the current description text

        :param description: description text
        :type description: string
        """
        # need to insert a check to validate allowed characters
        self.descriptionEdit.setText(description)

def main():
    app = QtGui.QApplication(sys.argv)
    loaderWindow = DescriptionGUI()
    loaderWindow.showUI()
    loaderWindow.setFocus()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
else:
	pass