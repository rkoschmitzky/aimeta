############## qthelper .py ###################################
# module includes some helper qt functions for Maya
#
#
# Creation Date:
# 2015-06-22
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'


# General Imports
import maya.OpenMayaUI as OpenMayaUI

# Package Imports
from aimeta.ui.qthelper import wrapinstance, QtGui


def getQMayaMainWindow():
    """ Return the QMainWindow for the Maya main Window """

    mainWin = OpenMayaUI.MQtUtil.mainWindow()
    if mainWin is None:
        raise RuntimeError('No Maya window found.')
    window = wrapinstance(mainWin)
    return window


def getQMayaMenuBar():
    """ Maya menubar for """

    for eachChild in getQMayaMainWindow().children():
        if type(eachChild) == QtGui.QMenuBar:
            return eachChild