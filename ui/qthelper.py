############## qthelper.py ###################################
# module handles the QT import for PySide and PyQt4
# code was taken from https://github.com/rgalanakis/practicalmayapython
#
# Creation Date:
# 2015-06-16
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'
__credits__ = ['Rico Koschmitzky', 'Robert Galanakis']

try:
    from PySide import QtCore, QtGui, QtWebKit
    import shiboken
    Signal = QtCore.Signal


    def _getcls(name):
        result = getattr(QtGui, name, None)
        if result is None:
            result = getattr(QtCore, name, None)
        return result

    def wrapinstance(ptr):
        """ Converts a pointer (in or long) into the concrete PySide/PyQt
            object it represents
        """

        ptr = long(ptr)
        qobj = shiboken.wrapInstance(ptr, QtCore.QObject)
        metaobj = qobj.metaObject()
        realcls = None
        while realcls is None:
            realcls = _getcls(metaobj.className())
            metaobj = metaobj.superClass()
        return shiboken.wrapInstance(ptr, realcls)

except ImportError:
    try:
        from PyQt4 import QtCore, QtGui, QtWebKit
        Signal = QtCore.pyqtSignal
        import sip
    
        def wrapinstance(ptr):
            return sip.wrapinstance(long(ptr), QtCore.QObject)
    except ImportError:
        raise ImportError('Neither PySide nor PyQt4 found.')

def getWidgetUnderCursor():
    """ returns the widget under mouse position

    """
    currentPos = QtGui.QCursor().pos()
    widget = QtGui.qApp.widgetAt(currentPos)
    return widget
