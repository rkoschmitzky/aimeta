__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'
__datatypes__ = ['int', 'float', 'string', 'point2', 'matrix']
__formats__ = ['exr']
__indicator__ = 'aiIndicator'