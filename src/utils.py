############## utils.py ###################################
# general helper functions
#
#
# Creation Date:
# 2015-06-18
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# generic imports
import json
import logging
import os
import platform
import re
import subprocess
import sys

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)


class ValidateAiDataType(object):
    """ includes a set of validators for the possible arnold datatypes format
    All of its methods are checking if a given string contains the correct formatting and types for the
    inquired datatype


    """
    def __init__(self):
        pass

    @staticmethod
    def int(string):
        """ checks if the given string includes exactly one integer value

        :param string: string to validate
        :return: True if valid, False if not
        """
        intMatches = map(str, re.findall(r'\d+', string))
        if len(intMatches) == 1:
            if len(intMatches[0]) == len(string):
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def string(string):
        """ checks if the given string only contains valid ascii characters

        :param string: string to validate
        :return: True if valid, False if not
        """
        try:
            string.decode('ascii')
            return True
        except UnicodeDecodeError:
            return False

    @staticmethod
    def float(string):
        """ checks if the given string includes exactly one float value

        :param string: string to validate
        :return: True if valid, False if not
        """
        # check for whitespaces
        if len(string.split(' ')) > 1:
            return False
        else:
            try:
                float(string)
                return True
            except ValueError:
                return False

    @staticmethod
    def point2(string):
        """ checks if the given string includes exactly two float values separated via whitespace

        :param string: string to validate
        :return: True if valid, False if not
        """
        if not len(string.split(' ')) == 2:
            return False
        else:
            for value in string.split(' '):
                if not ValidateAiDataType.float(value):
                    return False
            return True

    @staticmethod
    def matrix(string):
        """ checks if the given string includes exactly 16 float values separated via whitespace

        :param string: string to validate
        :return: True if valid, False if not
        """
        _splits = string.split(' ')
        if len(_splits) != 16:
            return False
        else:
            for element in _splits:
                if not ValidateAiDataType.float(element):
                    return False
            return True


def platformInfo():
    _system = {}
    _system['Node'] = platform.node()
    _system['OSRelease'] = platform.release()
    _system['OSVersion'] = platform.version()
    _system['Machine'] = platform.machine()
    return _system


def availableNetworkDrives():
    netDrives = dict()
    for line in subprocess.check_output(['net', 'use'], shell=False).splitlines():
        if line.startswith('OK'):
            fields = line.split()
            netDrives[fields[1]] = fields[2]
    return netDrives


def pathFromMappedDrive(path):
    _mappings = availableNetworkDrives()
    if _mappings != {}:
        rootLog.info('Found mapped drives.%s' % _mappings)
    _drive = os.path.splitdrive(path)[:1][0]
    if _drive in _mappings:
        path = path.replace(_drive, _mappings[_drive])
    return os.path.normpath(path)


def serializeData(filePath, data):
    """ writes a python dictonary via pickle module to a
        file
        :param filePath path including filename and file extension
        @type: string
        @param dict nested dictonary to serialize
        @type: {}
    """
    if not os.path.exists(os.path.dirname(filePath)):
        os.makedirs(os.path.dirname(filePath))
    with open(filePath, 'w+') as f:
        data = json.dumps(data, sort_keys = True, separators = (',', ':'))
        f.write(data)
        f.close()


def deserializeData(filePath):
    """ reads out a python dictonary viajson module from a
        file
        @param filePath path including filename and file extension
        @type: string
        @return unserialized data
        @type: dict
    """
    f = open(filePath, 'r')
    try:
        data = json.loads(f.read())
        return data
    except ValueError:
        return False


def addToData(filePath, key, value):
    """ adds Data to Json File

    @param filePath path to JSON file
    @type: string
    @param key: key Data
    @type: string
    @param value value Data
    @type: string or int or boolen or tuple or list or dict
    @return json file content
    @type: dict
    """
    if not os.path.exists(filePath):
        data = {}
    else:
        data = deserializeData(filePath)
    if not data:
        data = {}
    data[key] = value
    serializeData(filePath, data)
    return deserializeData(filePath)

def getFromData(filePath, key):
    """ gets Data from a Json file with a given key to look for

    :param filePath: filePath path to JSON file
    :param key: key to look for in the data
    :return: value if found, if not found None
    """
    data = deserializeData(filePath)
    if not data:
        data = {}
    if not data == {}:
        if key in data:
            return data[key]
        else:
            return None
    else:
        return {}