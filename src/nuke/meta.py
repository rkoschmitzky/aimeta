############## meta.py ###################################
# module includes the MetaObj class for handling metadata on
# rendered images
#
# Creation Date:
# 2015-09-11
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

import ast
import nuke
import logging
import sys

from aimeta.src.meta import Meta

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)

class MetaObj(Meta):


    def __init__(self, nodeName):
        # create class logger
        self._Log = logging.getLogger(name='%s - %s' % (__name__, self.__class__.__name__))
        self._Log.addHandler(handler)

        self.__node = nuke.toNode(nodeName)
        if self.__node is None:
            self._Log.error('Could not convert to node from name "%s"' % nodeName)
            return
        if not self._isValidNode():
            self._Log.error('Could not initialize. Needs a valid node class. Got "%s" instead.' % self.__node.Class())
            return
        else:
            self._Log.debug('Fully initialized %s from inputfile "%s"' % (self.__class__.__name__, self.getInputFilePath()))

        # init available metadata
        self.__currentData = self._getCurrentData()

    def getInputFilePath(self):
        """ checks input recursively and if the origin is a readnode print out the metadata

        :return:
        """
        _isValid = self._isValidNode()
        if _isValid:
            try:
                _fileName = self.__node.metadata()['input/filename']
                return _fileName
            except:
                return None

    def _isValidNode(self, node=None):
        """ checks if the internal node property or a given node is valit to be used within the class

        :param node: node to validate
        :type node: nuke node instance
        """
        if node is not None:
           try:
               _node = nuke.toNode(node)
               if self.__node.metadata()['input/bitsperchannel'] in self.imageFormats:
                   return True
               else:
                   return False
           except:
               return False
        elif self.__node.metadata()['input/bitsperchannel'] in self.imageFormats:
            return True

    def _getCurrentData(self):
        """ collects the data in a indexes dictonary including values in a way arnold reads it

        :return: dict
        """
        _data = {}
        index = 0
        for key, value in self.node.metadata().items():
            _value = self._getAiType(value)
            #if aiType or _value is not None:
            _data[index] = '%s %s %s' % (_value[1], key, value)
            index += 1
        return _data

    def update(self):
        """ refreshs the metadata

        """
        self.__currentData = self._getCurrentData()

    def _getAiType(self, value):
        """ reads the metadata from nukes metadata method and returns a list with
        the correct instance from its string representation, its arnold equivalent datatype and the real class type

        """

        if isinstance(value, dict):
            try:
                return [ast.literal_eval(value), 'STRING', 'dict']
            except:
                pass
        if isinstance(value, basestring):
            try:
                _correctedType = [float(_value) for _value in value.split(' ')]
                if len(_correctedType) == 2:
                    return [_correctedType, 'POINT2', 'list']
                if len(_correctedType) == 16:
                    return _correctedType, 'MATRIX', 'list'
            except:
                pass
            return value, 'STRING', 'basestring'
        if isinstance(value, list):
            try:
                _correctedType = [float(_value) for _value in value]
                if len(_correctedType) == 16:
                    return [_correctedType, 'MATRIX', 'list']
                if len(_correctedType) == 2:
                    return [_correctedType, 'POINT2', 'list']
            except:
                pass
        if isinstance(value, int):
            return [value, 'INT', 'int']
        if isinstance(value, float):
            return [value, 'FLOAT', 'float']
        return [value, None, None]

    #####################
    # getters & setters #
    #####################

    @property
    def node(self):
        """ returns the initialized node

        :return:
        """
        return self.__node

    @property
    def metaData(self):
        """ gets the metadata in the same way as the maya MetaObj class does

        :return: {#index: '#dataType #name # value'}
        """
        return self.__currentData