############## utils.py ###################################
# generel helper functions to work with Maya
#
#
# Creation Date:
# 2015-06-24
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# generic imports
import os
import logging
import sys

# maya imports
import pymel.core as pm
from pymel import versions

# package imports
from aimeta.src.utils import pathFromMappedDrive

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)


def mayaLocalLocation():
    """ gets the maya app dir location

    :return: if found it returns location, otherwise False
    :rtype: string or False
    """
    try:
        return os.getenv("MAYA_APP_DIR")
    except KeyError:
        return False

def mayaVersion():
    """ gets the used Maya Version

    :return: version number
    :rtype: string
    """
    return int(str(versions.current())[:4])


def scenePath(UNC=True):
    """ gets the current scene path

    :param UNC: if not set it will return the UNC path, if False it will return the mapped path
    :return: scene path
    :rtype: string
    """
    # have to find a way to insert UNC paths
    _path = pm.sceneName().abspath()
    return pathFromMappedDrive(_path)


def referencePaths():
    """ gets the paths of all references, this will use UNC paths if there are mapped drives

    :return: list with paths
    :rtype: [string]
    """
    return [pathFromMappedDrive(pm.referenceQuery(ref, filename = True)) for ref in pm.ls(rf = True)]


def alembicPaths():
    """ gets the paths of all alembic nodes, this will use UNC paths if there are mapped drives

    :return: list with paths
    :rtype: [string]
    """
    return [pathFromMappedDrive(os.path.abspath(abc.abc_File.get())) for abc in pm.ls(type = 'AlembicNode')]
