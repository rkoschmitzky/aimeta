############## datagui.py ###################################
# module includes the tool to work with exr metadata within
#  Maya/MtoA
#
# Creation Date:
# 2015-06-16
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'

# General Imports
import ast
from functools import partial
import logging
import os
import sys

# Package Imports
from aimeta.src.maya.meta import *
from aimeta.src.maya.utils import *
from aimeta.src.utils import *
from aimeta.ui.gui.datagui import DataGUI
from aimeta.ui.maya.qthelper import getQMayaMainWindow
from aimeta.ui.qthelper import QtGui, QtCore, Signal

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)

class DataGUIConnect(DataGUI):

    # tags to specify what metadata should be visible for the GUI and what not
    IGNORE_TAG = '.aimeta_'
    INDICATOR_TAG = 'indicator_'
    DESCRIPTION_TAG = '_description'

    def __init__(self, parent = getQMayaMainWindow()):
        super(DataGUIConnect, self).__init__(parent)

        self._Log = logging.getLogger(name='%s - %s' % (__name__, self.__class__.__name__))
        self._Log.addHandler(handler)
        self._Log.debug('Started DataGUI within %s' % (parent.objectName()))

        self.__drivers = listExrDriverFromAOVs()
        if self.__drivers is None:
            self._Log.warning('No AOVs or AOV-connected driver found.')
            return

        self._AOVs = getAOVs()
        self.__defaultMetaObj = MetaObj()
        self.__metaObj = MetaObj()
        self.__collectSettings = {}

        self.build(drivers=self.__drivers, AOVs=self._AOVs)
        self.appendMetaDataWidgets(self.IGNORE_TAG)
        try:
            self.loadSettings(self.defaultSettingsFile)
        except:
            self._Log.info('Could not load settings.')
        self.show()

        # keeps the queried metadata from UI
        self.__currentMetaData = self.metaData
        self._establishConnections()
        self.indicatorTab.bindMenuToIndicatorDropdowns(self._onBind)
        self.loadIndicators()

    def _establishConnections(self):
        """ connect the main signals

        """
        self.executeMetaDataButton.clicked.connect(self.setMetaData)
        self.metaDataTab.driverDropdown.currentIndexChanged.connect(partial(self.metaDataTab.onDriverChanged, self._onDriverChanged))
        self.executeSettingsButton.clicked.connect(self._onCollect)
        self.executeIndicatorsButton.clicked.connect(self.setIndicators)
        self.descriptionWindow.saveClicked.connect(self.saveDescription)

    def _onBind(self):
        """ function that will be used to query from what indicator the context menu will be called, to set the initial
        description correctly

        """
        if not os.path.exists(self.defaultSettingsFile):
            self.indicatorTab.initialDescription = 'No description found.'
        else:
            indicatorDescriptionTag = '%s%s%s' % (self.INDICATOR_TAG, self.indicatorTab.indicatorNameUnderPointer(), self.DESCRIPTION_TAG)
            _description = getFromData(self.defaultSettingsFile, indicatorDescriptionTag)
            if _description is None:
                self.indicatorTab.initialDescription = 'No description found.'
            else:
                self.indicatorTab.initialDescription = _description

    def setMetaData(self):
        """ sets the metadata queried from the custom metadata tab

        """
        # getting the metaData via names we want to remove
        _originNames = [value['name'] for key, value in self.__currentMetaData.items()]
        self._Log.debug('Queried old metadata entry names %s' % _originNames)
        _data = self.metaData
        self._Log.debug('Current metadata entry names from UI: %s' % _data)
        _newNames = [value['name'] for key, value in _data.items()]
        _namesToRemove = [member for member in _originNames if member not in _newNames]
        # removing the metaData thats names are not existing anymore
        if not _namesToRemove == []:
            self._Log.debug('Found names to remove from MetaData %s' % _namesToRemove)
            for name in _namesToRemove:
                self.__metaObj.remove(name)
        for key, value in _data.items():
            self._Log.debug('Item without pre-check found: %s' % key)
            if not value['name'] == '':
                self.__metaObj.add(value['name'], value['dataType'], value['value'])
        self.__currentMetaData = _data

    def appendMetaDataWidgets(self, ignoreTag):
        """ appends the metadata templates in the UI

        :param ignoreTag: tag to set which metadata should be hidden fronm the GUI
        :return:
        """
        for id in self.__metaObj.metaData:
            _name = self.__metaObj.getName(id)
            _dataType = self.__metaObj.getDataType(_name)
            _value = self.__metaObj.getValue(_name)
            # double check the dataType
            if _dataType in self.__metaObj.allowedDataTypes:
                if ignoreTag is not None and ignoreTag not in _name:
                    self.metaDataTab.addMetadataTemplate(_name, _dataType, _value)

    def setIndicators(self):
        """ sets the metadata queried by the UI

        """
        _indicatorTag = self.IGNORE_TAG + self.INDICATOR_TAG
        _indicators = list(set(self.indicators.values()))
        indicatorDict = {}
        for _indicator in _indicators:
            indicatorDict[_indicator] = [key for key, value in self.indicators.items() if value == _indicator]
        _MetaObjs = [MetaObj(obj) for obj in self.__drivers]
        for obj in _MetaObjs:
            for _indicator in indicatorDict:
                self.__metaObj.add(_indicatorTag + _indicator, 'string', indicatorDict[_indicator])

    def loadIndicators(self):
        """ loads the currently set indicators by reading the data from the default driver

        :return:
        """
        _indicatorTag = self.IGNORE_TAG + self.INDICATOR_TAG
        _hasIndicators, _indexes = self.__metaObj.hasInName(_indicatorTag)
        if not _hasIndicators:
            return
        for _index in _indexes:
            _name = self.__metaObj.getName(_index)
            try:
                # in case out metadata value is a list
                _indicator, _indicatorValue  = _name.split(_indicatorTag)[1], ast.literal_eval(self.__metaObj.getValue(_name))
                for _aov in _indicatorValue:
                    self.indicatorTab.setIndicatorDropDown(_aov, _indicator)
            except:
                # in case value is not a list were not having a defined indicator
                pass

    def _onCollect(self):
        self.storeSettings()
        self.autoCollect()

    def storeSettings(self):
        """ stores the states and values from the auto-collect tab to the settings file

        """

        if os.path.exists(self.defaultSettingsFile):
            self._Log.debug('Settings file found.')
            _settings = deserializeData(self.defaultSettingsFile)
            self._Log.debug('Queried original settings %s' % _settings)
            _currentSettings = self.collectorTab.getSettings(values=True)
            for key in _settings:
                try:
                    if _settings[key] != _currentSettings[key]:
                        _settings[key] = _currentSettings[key]
                except:
                    pass

            self._Log.debug('Queried new settings %s' % _settings)
            self.__collectSettings = _settings
        else:
            self.__collectSettings = self.collectorTab.getSettings(values=True)
            self._Log.debug('Queried Settings %s' % self.__collectSettings)
        try:
            self._Log.debug('Try to save settings to path %s' % self.defaultSettingsFile)
            serializeData(self.defaultSettingsFile, self.__collectSettings)
        except IOError:
            self._Log.error('Failed to save Settings')
            return False

    def autoCollect(self):
        """ executes the auto collection, means sets the metadata to all drivers

        :return:
        """
        if os.path.exists(self.defaultSettingsFile):
            try:
                self._Log.debug('Try to load settings from path %s' % self.defaultSettingsFile)
                _data = deserializeData(self.defaultSettingsFile)
            except IOError:
                self._Log.error('Failed to load settings from path %s' % self.defaultSettingsFile)
                return False

            # TODO: Test if the beauty AOV is always using the default arnold driver, it thats the case we only have to consider the default driver
            # creating the MetaObjs
            _MetaObjs = [MetaObj(obj) for obj in self.__drivers]

            # looking if we want to include the suffix in the custom metadata
            prefix = self.IGNORE_TAG
            if _data['prefix'] != '':
                prefix = prefix + _data['prefix'] + '_'

                for obj in _MetaObjs:
                    if _data['include_custom_metadata']:
                        self._Log.debug('Prefixing custom metadata')
                        self._prefixCustomMetadata(prefix, obj)

                    if _data['include_indicators']:
                        self._Log.debug('Prefixing indicators')
                        self._prefixIndicators(prefix, obj)

                        _descriptions = self._hasIndicatorDescription()
                        if _descriptions != False:
                            self._Log.debug('Reading and prefixing indicator descriptions.')
                            self._prefixDescriptions(prefix, _descriptions, obj)
                        else:
                            self._Log.debug('descriptions not found. Got ')

            # always add the suffix in the metadata name
            # adding the meta data
            # in case we do not pre cleaning before we should remove the metadata entry
            for obj in _MetaObjs:
                if prefix != self.INDICATOR_TAG:
                    obj.add('.aimetaautoprefix', 'string', prefix)
                else:
                    obj.remove()
                if _data['include_operator']:
                    obj.add(prefix + 'operator', 'string', '%s' % _data['operator'])
                else:
                    obj.remove(prefix + 'operator')
                if _data['include_project_name']:
                    obj.add(prefix + 'project_name', 'string', '%s' % _data['project_name'])
                else:
                    obj.remove(prefix + 'project_name')
                if _data['include_project_number']:
                    obj.add(prefix + 'project_number', 'string', '%s' % _data['project_number'])
                else:
                    obj.remove(prefix + 'project_number')
                if _data['include_scene_name']:
                    obj.add(prefix + 'maya_scene_name', 'string', '%s' % scenePath())
                else:
                    obj.remove(prefix + 'maya_scene_name')
                if _data['include_maya_version']:
                    obj.add(prefix + 'maya_version', 'string', '%s' % mayaVersion())
                else:
                    obj.remove(prefix + 'maya_version')
                if _data['include_os_infos']:
                    obj.add(prefix + 'os', 'string', '%s' % platformInfo())
                else:
                    obj.remove(prefix + 'os')
                if _data['include_alembic_paths']:
                    obj.add(prefix + 'alembic_paths', 'string', '%s' % alembicPaths())
                else:
                    obj.remove(prefix + 'alembic_paths')
                if _data['include_maya_reference_paths']:
                    obj.add(prefix + 'reference_paths', 'string', '%s' % referencePaths())
                else:
                    obj.remove(prefix + 'reference_paths')
        else:
            return False

    def loadSettings(self, filePath):
        """ alters the auto-collect data widgets by reading out the settings file

        :param filePath: path the the settings file
        :type filePath: string
        """
        if os.path.exists(filePath):
            _data = deserializeData(filePath)
            self.__collectSettings = self.collectorTab.getSettings()
            # for now we have to try: except: to differ the correct way to set the values
            for key in _data:
                # setting the values of the lineEdit widgets
                try:
                    self.__collectSettings[key].setText(_data[key])
                except:
                    pass
                # setting the CheckBox states
                try:
                    if _data[key]:
                        self.__collectSettings[key].setCheckState(QtCore.Qt.CheckState(2))
                    if not _data[key]:
                        self.__collectSettings[key].setCheckState(QtCore.Qt.CheckState(0))
                except:
                    pass
        else:
            self._Log.warning('No settings file found.')

    def _prefixCustomMetadata(self, prefix, metaObjInstance):
        """ copies the found custom metadata, adds the prefix to it and creating new hidden metadata from it

        :param prefix: prefix
        :type prefix: string
        :param metaObjInstance: MetaObj class instance
        :return:
        """
        names = [metaObjInstance.getName(index) for index in metaObjInstance.metaData]
        self._addPrefixedData(prefix, names, metaObjInstance)

    def _prefixIndicators(self, prefix, metaObjInstance):
        """ copies the found indicators, adds the prefix to it and creating new hidden metadata from it

        :param prefix: prefix
        :type prefix: string
        :param metaObjInstance: MetaObj class instance
        :return:
        """
        _indicatorTag = self.IGNORE_TAG + self.INDICATOR_TAG
        names = self._getSetIndicators()
        if names is None:
            return
        self._Log.debug('Found set Indicators "%s"' % names)
        _toRemove = [name for name in names if not _indicatorTag in name]
        for name in _toRemove:
            self.__defaultMetaObj.remove(name)
        _toAdd = [prefix + name.split(self.IGNORE_TAG)[1] for name in self._getSetIndicators()]
        if _toAdd is not []:
            self._addPrefixedData(prefix, self._getSetIndicators(), metaObjInstance, False)

    def _prefixDescriptions(self, prefix, descriptionDict, metaObjInstance):
        """

        :param prefix:
        :param descriptionDict:
        :param metaObjInstance:
        :return:
        """
        _descriptionsFound = self._getSetDescriptions()
        if _descriptionsFound is not None:
            for name in self._getSetDescriptions():
                self.__defaultMetaObj.remove(name)
        for key, value in descriptionDict.items():
            metaObjInstance.add(prefix+key, 'string', value)

    def _addPrefixedData(self, prefix, entryNames, metaObjInstance, ignoreTagged=True):
        """ prefix the given entrynames

        :param prefix: prefix
        :param entryNames: metadata names
        :param metaObjInstance: MetaObj class instance
        :param ignoreTagged: if not set it's True by default and will ignore data with a specific string in name,
        if set to False it will all the data
        """
        data = [[metaObjInstance.getDataType(name), metaObjInstance.getValue(name) ] for name in entryNames]
        _dataDict = {}
        _idx = 0
        for name in entryNames:
            if ignoreTagged:
                if not self.IGNORE_TAG in name and name != '':
                    metaObjInstance.add(prefix+name, data[_idx][0], data[_idx][1])
            else:
                # TODO: find a better way to keep out a previously set prefix and exchange that with the new one
                metaObjInstance.add(prefix+name.split(self.IGNORE_TAG)[1], data[_idx][0], data[_idx][1])
            _idx += 1

    def _getSetIndicators(self):
        """ gets all the metadata entry names that are defined as indicators

        :return: list with metadata entry names
        :rtype: list
        """
        # updating the default MetaObj, because it may have changed
        self.__defaultMetaObj.update()
        # because the defaultArnoldDriver is not deletable we will use it to query the indicators
        _hasHidden, _hIndexes = self.__defaultMetaObj.hasInName(self.IGNORE_TAG)
        _hasIndicators, _iIndexes = self.__defaultMetaObj.hasInName(self.INDICATOR_TAG)
        _hasDescriptions, _dIndexes = self.__defaultMetaObj.hasInName(self.DESCRIPTION_TAG.split('_')[1])
        if _hasIndicators and _hasHidden:
            _indexes = [_index for _index in _iIndexes if _index in _hIndexes]
        else:
            return None
        # keep the indicator description out
        if _hasIndicators:
            if _hasDescriptions == False:
                return [self.__defaultMetaObj.getName(_index) for _index in _indexes]
            else:
                return [self.__defaultMetaObj.getName(_index) for _index in _indexes if not _index in _dIndexes]
        else:
            return None

    def _getSetDescriptions(self):
        """ gets all the metadata entry names that are defined as descriptions

        :return:
        """
        # updating the default MetaObj, because it may have changed
        self.__defaultMetaObj.update()
        _hasHidden, _hIndexes = self.__defaultMetaObj.hasInName(self.IGNORE_TAG)
        _hasDescriptions, _indexes = self.__defaultMetaObj.hasInName(self.DESCRIPTION_TAG.split('_')[1])
        if _hasDescriptions:
            return [self.__defaultMetaObj.getName(_index) for _index in _indexes if _index in _hIndexes]
        else:
            return None

    def _onDriverChanged(self):
        """ called when the driver changes in the QComboBox

        """
        self._setUsedDriver()
        self.appendMetaDataWidgets(self.IGNORE_TAG)
        self.__currentMetaData = self.metaData

    def _setUsedDriver(self):
        """ initializing the MetaObj with the current set driver

        """
        self.__metaObj = MetaObj(self.metaDataTab.currentDriver)

    def _hasIndicatorDescription(self):
        """

        :return:
        """
        _data = {key: value for key, value in deserializeData(self.defaultSettingsFile).items() if self.INDICATOR_TAG and self.DESCRIPTION_TAG in key}
        return _data

    def saveDescription(self, uiData):
        """ saves the description edited in the Description GUI and stores it within the settings file

        :param uiData: data including a tuple of the indicator name and its description text
        :type uiData: string, string
        """
        indicatorName, description = uiData
        _descriptionKey = '%s%s%s' % (self.INDICATOR_TAG, indicatorName, self.DESCRIPTION_TAG)

        self.__collectSettings = self.collectorTab.getSettings(values=True)
        self.storeSettings()
        addToData(self.defaultSettingsFile, _descriptionKey, description)

        self._Log.debug('Adding indicator description to metadata')
        self._Log.info('description added %s %s' % (indicatorName, description))

    @property
    def defaultSettingsFile(self):
        """ holds the path to the default .json settings file

        :return:
        """
        return os.path.join(os.path.normpath(mayaLocalLocation()), '.aiMeta', 'settingsMaya.json')



