############## meta.py ###################################
# module includes the MetaObj class and additional functions
# for handling exr metadata workflows within Maya/MtoA
#
# Creation Date:
# 2015-06-16
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# generic imports
import pymel.core as pm
import logging
import sys

from aimeta.src.meta import Meta

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)

class MetaObj(Meta):

    SPECS = {'int': 'INT name value',
             'float': 'FLOAT name value' ,
             'string': 'STRING name value',
             'point2': 'POINT2 name value',
             'matrix': 'MATRIX name value'
            }

    def __init__(self, driver=None):
        # create class logger
        self._log = logging.getLogger(name='%s - %s' % (__name__, self.__class__.__name__))
        self._log.addHandler(handler)
        self._log.debug('Initialize %s from driver named %s' % (self.__class__.__name__, driver))

        if driver:
            self._log.info('Check if driver %s is valid.' % driver)
            if self._isValid(driver):
                self._log.info('%s is valid. Create MetaObj.' % driver)
                self._driver = pm.PyNode(driver)
            else:
                self._log.error('Driver %s is not valid.' % driver)
                return
        else:
            # try to initialize default driver
            driver = getDefaultDriver()
            if self._isValid(driver):
                self._driver = driver
                self._log.info('MetaObj initialized without driver name. Initialized with default driver successfully.')
            else:
                self._log.error('No arnold default driver found. Please declare a driver.')
                return

        # declaring private members
        self.__metaDataSpecs = self.SPECS
        # init available metadata
        self.__currentData = self._getCurrentData()

    def _isValid(self, driver):
        """ checks if the the param driver gets a aiAOVDriver with a valid exr image format

        :param driver: driver node
        :return: True if driver is valid, False if not
        :rytpe: bool
        """
        if not isinstance(driver, pm.PyNode):
            try:
                driver = pm.PyNode(driver)
            except:
                self._Log.error('Not able to genereate PyNode from potential driver %s' % driver)
                return False

        if not driver.type() == 'aiAOVDriver':
            self._log.error('%s is not valid. Please choose a valid aiAOVDriver node.' % self._driver)
            if _imageFormat(driver) not in self.imageFormats:
                self._log.error('has to use exr as image format. Got %s instead' % _imageFormat(driver))
                return False
        else:
            return True

    def _isAllowed(self, dataType):
        """ checks if the datyType is allowed or not

        :param datyType: type of data you want to add. Supported types are 'int', 'float', 'point2', 'string', 'matrix'
        :return: True if found in the allowed types, False if not
        :rtype: bool
        """
        if not dataType in self.allowedDataTypes:
            self._log.error('%s are allowed dataTypes. Got "%s" instead' % (', '.join(self.allowedDataTypes), dataType))
            return False
        else:
            return True

    def clear(self, nameOnly=False, name=None):
        """ deletes all the metadata

        :param nameOnly: if set to True it will delete only metadata including a specific string in name
        :type nameOnly: bool
        :param name: if the nameOnly keyword is set to True the stringInName has to be the string to look for
        in the metadata names
        :type name: string
        :return:
        """
        assert isinstance(nameOnly, bool)
        if nameOnly and name is None:
            self._log.error('You have to declare the name keyword and use it with a string.')
        if nameOnly and name is not None:
            self.remove(name, False)
        if not nameOnly:
            indexes = [i for i in range(0, self._getLastIndex() + 1)]
            self._removeByIndex(indexes)

    def replace(self, name, newDataType, newValue, newName=None):
        """ replaces the dataType and value of a given metadata entry name

        :param name: metadata entry name, it's value and datatype will be replaced
        :param dataType: type of data you want to add. Supported types are 'int', 'float', 'point2', 'string', 'matrix'
        :type dataType: string
        :param value: the value that should be assigned to the entry name
        :type value: depends on the dataType, string, int, bool or float
        :param newName: if set it also replaces the metadata entry name with the new name
        :type newName: string
        :return: False if data type is unknown
        :rtype: bool
        """
        _index = self.remove(name, True)
        if _index is None:
            return False
        if not newDataType in self.allowedDataTypes:
            self._log.error('Replacing %s needs a valid dataType. Got "%s" instead. Allowed are %s' % (name, newDataType, self.allowedDataTypes))
            return False
        if newName is not None:
            assert isinstance(newName, basestring)
            name = newName
        if isinstance(_index, list):
            _index = _index[0]
            self._log.warning('Found multiple indexes to replace with. Choosen index "%s" automatically.' % _index[0])
        self.add(name, newDataType, newValue, index=_index)
        self.update()

    def add(self, name, dataType, value, index=None):
        """ adds the metadata to the driver

        :param name: metadata entry name
        :type name: string
        :param dataType: type of data you want to add. Supported types are 'int', 'float', 'point2', 'string', 'matrix'
        :type dataType: string
        :param value: the value that should be assigned to the entry name
        :type value: string
        :param index: (optional) allows you to add data at a specific index
        :type index: int
        :return: True if successful, False if not
        :rtype: bool
        """
        _nameExists, _index = self.hasName(name)
        if not self._isAllowed(dataType):
            self._log.error('Given datatype "%s" not found in %s' % (dataType, self.allowedDataTypes))
            return False
        if index is None:
            if _nameExists:
                index = _index
                self._log.info('Name "%s" already found. Will replace existing data.' % name)
            else:
                index, free = self._getFreeIndex()
                self._log.debug('Found %s free indexes on driver %s' % (free, self._driver))
                if free is None:
                    index = self._getNonUsedIndex()
                else:
                    index = index[0]
        else:
            if _nameExists:
                self._log.error('Skipped metadata addition, because name "%s" already found at index %s' % (name, _index))
                return False
            else:
                index = index
        # adding the new value
        _splits = self.metaDataSpecs[dataType].split(' ')
        attrToSet = '%s %s %s' % (_splits[0], name, value)
        try:
            pm.setAttr('%s.customAttributes[%s]' %(self._driver, index), attrToSet)
            self._log.info('Set %s to %s. .customAttributes at index %s' % (attrToSet, self._driver, index))
        except:
            self._log.error('Could not add "%s" to %s. .customAttributes at index %s' % (attrToSet, self._driver, index))
        self.update()

    def remove(self, name, exactMatch=True):
        """ removes metadata entry by a given name if it exists

        :param name: metadata entry name
        :type name: string
        :param exactMatch: if True it will remove when the name matches exactly, if False it looks for the string
        inside a name
        :type exactMatch: bool
        """
        assert isinstance(exactMatch, bool)
        if exactMatch:
            hasName, indexes = self.hasName(name)
        else:
            hasName, indexes = self.hasInName(name)
        if hasName:
            self._removeByIndex(indexes)
        else:
            self._log.warning('Name %s not found. Nothing to remove' % name)
            #return None, index
        self.update()
        if hasName:
            return indexes
        else:
            return None

    def update(self):
        self.__currentData = self._getCurrentData()

    def _removeByIndex(self, indexes):
        """ removes metadata by a given index

        :param indexList: list containing the indexes or single index that should be removed
        :type indexList: list or int
        """
        if isinstance(indexes, int):
            indexes = [indexes]
        for index in indexes:
            try:
                pm.setAttr('%s.customAttributes[%s]' %(self._driver, index), '')
                self._log.debug('Removed metadata at given index "%s"' % index)
            except:
                self._log.error('Could not remove metadata from index' % index)
        self.update()

    def _getCustomAttributeIndex(self, name):
        index = [key for key, value in self.metaData.items() if self._name(value) == name]
        return index

    def _getFreeIndex(self):
        """ gets the index or indexes that exists in the existing customAttribues multiInstances that are empty

        :return: indexes and the count of the free indexes
        :rtype: (list, int)
        """
        _data = self.metaData
        foundIndex = [key for key, value in _data.items() if self._name(value) == '']
        if foundIndex == []:
            return [], None
        else:
            return foundIndex, len(foundIndex)

    def _getCurrentData(self):
        """ collects the data in a indexes dictonary including values in a way arnold reads it

        :return: dict
        """
        _data = {}
        index = 0
        for data in self._driver.customAttributes.get():
            _data[index] = data
            index += 1
        return _data

    def _getNonUsedIndex(self):
        return len(self._driver.customAttributes.get())

    def _getLastIndex(self):
        return max(self.metaData.keys())

    #####################
    # getters & setters #
    #####################

    @property
    def metaDataSpecs(self):
        return self.__metaDataSpecs

    @property
    def metaData(self):
        """ holds the set metadata for the object

        :return: dictonary including the index as key and its data als unicode
        :rtype: unicode
        """
        return self.__currentData

    @property
    def driver(self):
        """ holds the name of the initialized driver

        :return: driver name
        :rtype string
        """
        return self._driver


def getDriver(AOVNode):
    """ gets the connected driver
    this is supposed not to work for multiple drivers per AOV

    :param AOVNode: AOV node you want to know the connected driver
    :type AOVNode: PyNode
    :return: returns the aiAOVDriver node if True or False if there is no driver connected
    :rtype: PyNode or bool
    """
    for attr in AOVNode.listAttr():
        try:
            if attr.inputs()[0].type() == 'aiAOVDriver':
                rootLog.debug('Driver "%s" connection found for AOV: %s' % (attr.inputs()[0].type(), AOVNode))
                return attr.inputs()[0]
        except:
            pass
    return False


def getAOVs(hasIndicator=False, indicatorAttrName='aiIndicator'):
    """ lists all available aiAOV nodes in the scene

    :return: list of AOVs nodes, if there is no aiAOV node it will return False
    :rtype: list or bool
    """
    _aovs = pm.ls(type = 'aiAOV')
    # has Indicator is obsolete at the moment
    # it should use the MetaObj instance and query the indicator from there
    if hasIndicator:
        _aovs = [aov for aov in _aovs if aov.hasAttr(indicatorAttrName)]
    if _aovs == []:
        rootLog.warning('No MtoA related AOVs found.')
        return False
    else:
        rootLog.debug('aiAOVS found: %s ' % _aovs)
        return _aovs


def listExrDriverFromAOVs():
    """ lists every available aiAOVDriver nodes which have a connection to the AOVs used
    in the scene

    :return: aiAOVDriver nodes
    :rtype: list
    """
    _drivers = []
    if not getAOVs():
        return None
    else:
        for aov in getAOVs():
            rootLog.debug('%s' % aov)
            if not getDriver(aov):
                rootLog.info('Skipped %s, seems to have no driver' % aov)
            else:
                _drivers.append(getDriver(aov))
    if not _drivers == []:
        return list(set(_drivers))
    else:
        return None


def getDefaultDriver():
    """ returns the default 'defaultArnoldDriver' if found, otherwise None

    :return: PyNode or None
    """
    try:
        return pm.PyNode('defaultArnoldDriver')
    except:
        return None


def _imageFormat(aiDriverNode):
    """ get the value of current translator attribute, used to query the image format that will be
    used for rendering

    :param AOVNode: name of the AOVNode
    :type AOVNode: PyNode
    :return: value of the aiTranslator attribute, e.g.: 'exr', 'jpeg', 'png'
    :rtype: unicode
    """
    return aiDriverNode.aiTranslator.get()