############## meta.py ###################################
# module includes a base Meta class to inherite from
#
#
# Creation Date:
# 2015-06-16
# author: Rico Koschmitzky
# email: contact@ricokoschmitzky.com
#
#

__author__ = 'Rico Koschmitzky'
__email__ = 'contact@ricokoschmitzky.com'

# generic imports
import logging
import sys

# initialize logging
rootLog = logging.getLogger(__name__)
rootLog.setLevel(logging.DEBUG)
rootLog.propagate = 0

handler = logging.StreamHandler(sys.__stderr__)
rootLog.addHandler(handler)

logFormatter = logging.Formatter('%(levelname)-8s | [aiMeta] |%(name)s | %(message)s')
handler.setFormatter(logFormatter)

class Meta(object):

    ALLOWED_DATA_TYPES = ['int', 'float', 'string', 'point2', 'matrix']
    ALLOWED_FORMATS = ['32-bit float', '16-bit half float']

    # TODO add a method to get the Value and Datatype by index in the meta class

    def __init__(self):
        # create class logger
        self._Log = logging.getLogger(name='%s - %s' % (__name__, self.__class__.__name__))
        self._Log.addHandler(handler)
        self._Log.debug('Initialize %s' % self.__class__.__name__)

        # declaring private members
        self.__allowedFormats = self.ALLOWED_FORMATS

    def hasName(self, name):
        """ checks if the metadata name already exists, we want to use this to
        ensure we have always unique metadata names or to replace existing metadata entries

        :param name: metadata entry name
        :type name: string
        :return: False name doesn't exist, True if if exists and given index of the customAttributes
        :rtype: bool or (bool, int)
        """
        assert isinstance(name, basestring)
        _data = self.metaData
        _foundNames = [value for key, value in _data.items() if self._name(value) == name]
        if _foundNames == []:
            return False, 0
        else:
            return True, [key for key, value in _data.items() if value == _foundNames[0]][0]

    def hasInName(self, searchString):
        """ checks if the metadata including a sprecific string already exists

        :param searchString: metadata entry content to look for
        :type searchString: string
        :return: False if nothing was found, True if if exists and a list with metadata entry names, when found
        :rtype: bool or (bool, int)
        """
        assert isinstance(searchString, basestring)
        _data = self.metaData
        _foundNames = [value for key, value in _data.items() if searchString in self._name(value)]
        if _foundNames == []:
            return False, 0
        else:
            _indexes = [key for key, value in _data.items() if _data[key] in _foundNames]
            return True, _indexes

    def getName(self, metaDataIndex):
        """

        :param metaDataIndex:
        :return:
        """
        _found = [self._name(value) for key, value in self.metaData.items() if key == metaDataIndex]
        if _found == []:
            self._Log.warning('Index %s does not exist or value was not found')
        else:
            return _found[0]

    def getDataType(self, name):
        """ returns the set dataType by a given name if found

        :param name: metadata entry name
        :type name: string
        :return: dataType if found otherwise False
        """
        assert isinstance(name, basestring)
        _hasName, _index = self.hasName(name)
        if _hasName:
            try:
                return self.metaData[_index].split(' ')[0].lower()
            except KeyError:
                self._Log.debug('Seems that the index is not included in .metaData. Got index %s' % _index)
        else:
            self._Log.warning('Tried to receive datatype, but no metaData with name "%s" found.' % name)

    def getValue(self, name):
        """ returns the set dataType by a given name if found

        :param name: metadata entry name
        :type name: string
        :return: dataType if found otherwise False
        """
        assert isinstance(name, basestring)
        _hasName, _index = self.hasName(name)
        if _hasName:
            try:
                return self.metaData[_index].split(' ', 2)[-1]
            except KeyError:
                self._Log.debug('Seems that the index is not included in .metaData. Got index %s' % _index)
        else:
            self._Log.warning('Tried to receive value, but no metaData with name "%s" found.' % name)

    def _name(self, value):
        """ gets the metadata attribute name from obj.metaData

        :param value: metadata unicode element
        :type value: unicode
        :return: metadata attribute name
        :rtype: string
        """
        try:
            return value.split(' ')[1]
        except:
            #self._Log.debug('Could not split entry "%s". Skipped.' % value)
            return ''

    @property
    def metaData(self):
        """ placeholder property

        :return:
        """

        return {}

    @property
    def imageFormats(self):
        """ holds the currently allowed image formats

        :return: image format
        :type: string
        """
        return self.ALLOWED_FORMATS

    @property
    def allowedDataTypes(self):
        """ holds the currently allowed dataTypes

        :return: dataTypes
        :rtype: string
        """
        return self.ALLOWED_DATA_TYPES