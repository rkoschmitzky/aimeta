# aiMeta

## abstract
aiMeta delivers an interface to handle exr metadata within Maya and the MayaToArnold plugins and to read those
metadata within Nuke in a more userfriendly and uniform way. It extends the ways of handling such data where MtoA
lacks at the current point.

This interface within Maya allows you to:

* add and remove any custom data to connected aiAOVDriver nodes
* defining indicators on aiAOV nodes and provide indicator descriptions
* gather and append scene and os specific data, as for example alembic paths and reference paths

The tool makes use of simple custom API using a MetaObj class that handles metadata modifications on aiAOVDriver nodes.

----
## installation

Note that the current installation is only for a demonstration purpose. It could be improved for a proper usage.
For now download or clone the repository and rename the root directory of it *aimeta*.
Edit your Maya.env and add/extend the MAYA_MODULE_PATH and let it point to the  *aimeta* folder.


----
## Examples

This example shows how to start the aiMeta user interface in Maya (Please set some AOVs to active in the rendersettings so you
to see the AOVs and their connected driver in the interface)
```python
from aimeta.src.maya.connect.datagui import DataGUIConnect

win = DataGUIConnect()
win.showUI()

```

Example shows the usage of the MetaObj class in Maya:

```python
from aimeta.src.maya.meta import MetaObj


>>> x = MetaObj()
>>> print 'data: %s' % x.metaData

'data: {}'

>>> print 'MetaObj instance was initialized using %s' % x.driver

MetaObj instance was initialized using defaultArnoldDriver

>>> x.add('firstTest', 'string', 'hello world')
>>> print 'data: %s' % x.metaData
data: {0: u'STRING firstTest hello world'}

>>> x.add('secondTest', 'int', 2)
>>> print 'Has name "Test" in data: %s at index %s' % (x.hasName('Test'))
Has name "Test" in data: False at index 0

>>> print 'Contains namestring "second" in data: %s at indexes %s' % (x.hasInName('second'))
Contains namestring "second" in data: True at indexes [1]

>>> x.replace('firstTest', 'float', 1.0)
>>> print 'data: %s' % x.metaData
data: {0: u'FLOAT firstTest 1.0', 1: u'INT secondTest 2'}

>>> print 'name at index 1 is: %s' % x.getName(1)
name at index 1 is: secondTest

>>> print 'dataType for name "secondTest" is: %s' % x.getDataType('secondTest')
dataType for name "secondTest" is: int

>>> print 'value for name "firstTest" is: %s' % x.getValue('firstTest')
value for name "firstTest" is: 1.0

>>> x.clear()
>>>print 'data %s' % x.metaData
data {0: u'', 1: u''}
```

----
## objective

This project is for demonstration purpose only. It is in a rough state and not really usable at the moment.
In the long run besides an interface for Maya it should also provide an interface for Nuke to show stored metadata via a custom panel.
This custom panel should also be able to allocate custom python scripts per indicator and to show the indicator descriptions
for the Compositing Artist provided by the Lighting TD.
A proper example usage for it would be to automate rebuilding of beauty passes or applying any action on passes (their readNodes)
by using a declared indicator instead of using naming conventions.